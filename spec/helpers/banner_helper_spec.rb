require 'rails_helper'

RSpec.describe BannerHelper, type: :helper do
  
  describe "banner_hml_code" do
    it "render link with image" do
      campaign = create :campaign_with_user
      expect(banner_hml_code(campaign.id)).to match(Regexp.new(click_banner_url(campaign.id)))
      expect(banner_hml_code(campaign.id)).to match(Regexp.new(root_url + "uploads/campaign/image/#{campaign.id}/banner.gif"))
    end
  end

end
