require 'rails_helper'

RSpec.describe ApplicationHelper, :type => :helper do
  
  describe "flash messages" do
    it "show message using bootstrap alert class" do
      flash[:error] = "error"
      expect(flash.count).to eq(1)
      expect(flash_message).to match(/alert/)
    end

    it "replace key from notice to success" do
      flash[:notice] = "message"
      expect(flash_message).to match(/alert-success/)
    end

    it "replace key from alert to danger" do
      flash[:alert] = "message"
      expect(flash_message).to match(/alert-danger/)
    end
  end

end
