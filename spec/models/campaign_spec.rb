require 'rails_helper'

RSpec.describe Campaign, type: :model do
  
  it "generated cache_key" do
    campaign = stub_model(Campaign, id: 5)
    expect(campaign.cache_key).to eq("campaigns:5")
  end

  describe "#get_counts" do
    before(:each) do
      @campaign = create :campaign_with_user
    end

    it "get counts from cache" do
      4.times { Campaign.inc_views @campaign.id }
      2.times { Campaign.inc_hits @campaign.id }
      counts = @campaign.get_counts
      expect(counts[:views]).to eq(4)
      expect(counts[:hits]).to eq(2)
      expect(counts[:conversion]).to eq(50)
    end

    it "avoid division by zero" do
      Campaign.inc_hits @campaign.id
      counts = @campaign.get_counts
      expect(counts[:views]).to eq(0)
      expect(counts[:hits]).to eq(1)
      expect(counts[:conversion]).to eq(100)
    end

    it "get zero values" do
      counts = @campaign.get_counts
      expect(counts[:views]).to eq(0)
      expect(counts[:hits]).to eq(0)
      expect(counts[:conversion]).to eq(0)
    end
  end

  describe "callbacks" do
    before(:each) do
      @campaign = create :campaign_with_user
    end

    describe "after create" do
      it "create cache record" do
        expect(Cache.redis.exists("campaigns:#{@campaign.id}")).to be true
      end

      it "add id to banner_list" do
        ids = Cache.redis.smembers "campaigns_list"
        expect(ids).to include(@campaign.id.to_s)
      end
    end

    describe "after update" do
      it "update cached link" do
        @campaign.update_attributes link: "http://domain/new_link"
        expect(Cache.redis.hget("campaigns:#{@campaign.id}", "link")).to eq("http://domain/new_link")
      end
    end

    describe "after destroy" do
      it "delete cached record" do
        @campaign.destroy
        expect(Cache.redis.exists("campaigns:#{@campaign.id}")).to be false
      end

      it "remove id from banner_list" do
        @campaign.destroy
        ids = Cache.redis.smembers "campaigns_list"
        expect(ids).not_to include(@campaign.id.to_s)
      end
    end
  end

  describe "class methods" do
    before :each do
      @campaign = create :campaign_with_user
    end

    it "random_id return integer value > 0" do
      id = Campaign.random_id
      expect(id).to be > 0
      expect(id).to be_a Integer
    end

    it "inc_views increment views count" do
      expect {
        Campaign.inc_views @campaign.id
      }.to change {
        Cache.redis.hget("campaigns:#{@campaign.id}", "views").to_i
      }.by(1)
    end

    it "inc_hits increment hits count" do
      expect {
        Campaign.inc_hits @campaign.id
      }.to change {
        Cache.redis.hget("campaigns:#{@campaign.id}", "hits").to_i
      }.by(1)
    end

    it "get_link get campaign link from redis" do
      expect(Campaign.get_link(@campaign.id)).to eq(@campaign.link)
    end
  end

end

