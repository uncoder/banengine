require 'rails_helper'

RSpec.describe CampaignsController, :type => :controller do

  describe "logged in" do
    before(:each) do
      @user = create :user
      sign_in @user
    end

    describe "GET #index" do
      it "responds successfuly" do
        get :index
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it "renders index template" do
        get :index
        expect(response).to render_template(:index)
      end

      it "assign current users campaigns" do
        create :campaign, user: @user
        get :index
        expect(assigns(:campaigns)).to eq(@user.campaigns)
        expect(assigns(:campaigns).count).to eq(1)
      end
    end

    describe "GET #new" do
      it "responds successfuly" do
        get :new
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it "build new campaign" do
        get :new
        expect(assigns(:campaign)).to be_a_new(Campaign)
      end

      it "render new new template" do
        get :new
        expect(response).to render_template(:new)
      end
    end

    describe "POST #create" do
      it "create new campaign" do
        expect {
          post :create, campaign: { name: "name", link: "http://domain/link", image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'banner.gif'), 'text/jpg') }
        }.to change{Campaign.count}.by(1)
      end

      it "redirect to index when success" do
        post :create, campaign: { name: "name", link: "http://domain/link", image: Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec', 'support', 'banner.gif'), 'text/jpg') }
        expect(response).to redirect_to(campaigns_path)
      end

      it "render form when unsuccess" do
        post :create, campaign: { name: "name", link: "http://domain/link" }
        expect(response).to render_template(:new)
      end
    end

    describe "GET #edit" do
      before(:each) do
        @campaign = create :campaign, user: @user
      end

      it "responds successfuly" do
        get :edit, id: @campaign.id
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it "assign campaign" do
        get :edit, id: @campaign.id
        expect(assigns(:campaign)).to eq(@campaign)
      end

      it "reject other user campaign" do
        other_user = create :user
        other_campaign = create :campaign, user: other_user
        expect{
          get :edit, id: other_campaign.id  
        }.to raise_error
      end
    end

    describe "PUT #update" do
      before(:each) do
        @campaign = create :campaign, user: @user, name: '1name'
      end

      it "update campaign" do
        expect {
          put :update, id: @campaign.id, campaign: { name: "2name" }
        }.to change{@campaign.reload.name}.from("1name").to("2name")
      end

      it "redirect to index when success" do
        put :update, id: @campaign.id, campaign: { name: "2name" }
        expect(response).to redirect_to(campaigns_path)
      end

      it "render form when unsuccess" do
        put :update, id: @campaign.id, campaign: { name: "2name", link: "" }
        expect(response).to render_template(:edit)
      end
    end

    describe "DELETE #destroy" do
      before(:each) do
        @campaign = create :campaign, user: @user
      end

      it "delete campaign" do
        expect {
          delete :destroy, id: @campaign.id
        }.to change{Campaign.count}.by(-1)
      end

      it "redirect to index" do
        delete :destroy, id: @campaign.id
        expect(response).to redirect_to(campaigns_path)
      end
    end

    describe "GET #show" do
      before(:each) do
        @campaign = create :campaign, user: @user
      end

      it "responds successfuly" do
        get :show, id: @campaign.id
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end

      it "renders show template" do
        get :show, id: @campaign.id
        expect(response).to render_template(:show)
      end

      it "assign campaign" do
        get :show, id: @campaign.id
        expect(assigns(:campaign)).to eq(@campaign)
      end
      
      it "assign counts" do
        get :show, id: @campaign.id
        expect(assigns(:counts)).to eq(@campaign.get_counts)
      end
    end

  end

  describe "not logged in" do
    it "responds with redirect to auth page" do
      get :index
      expect(response).to redirect_to(new_user_session_path)
    end
  end

end
