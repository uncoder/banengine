require 'rails_helper'

RSpec.describe BannerController, :type => :controller do

  describe "GET #index" do
    before(:each) do 
      create :campaign_with_user
    end

    it "responds successfuly" do
      get :index, format: :js
      expect(response).to be_success
      expect(response).to have_http_status(200)
      expect(response.header['Content-Type']).to include('text/javascript')
    end

    it "renders index template" do
      get :index, format: :js
      expect(response).to render_template(:index)
    end

    it "assign id" do
      get :index, format: :js
      expect(assigns(:id)).to be > 0
      expect(assigns(:id)).to be_an(Integer)
    end

    it "call inc_views" do
      allow(Campaign).to receive(:inc_views)
      get :index, format: :js
      expect(Campaign).to have_received(:inc_views)
    end
  end

  describe "GET #click" do
    before(:each) do 
      @campaign = create :campaign_with_user
    end

    it "redirect to campaign url" do
      get :click, id: @campaign.id
      expect(response).to redirect_to(@campaign.link)
    end

    it "raise error when campaign not found" do
      expect{
        get :click, id: 0
      }.to raise_error
    end

    it "call inc_hits" do
      allow(Campaign).to receive(:inc_hits)
      get :click, id: @campaign.id
      expect(Campaign).to have_received(:inc_hits)
    end
  end

end
