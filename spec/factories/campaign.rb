FactoryGirl.define do
  factory :campaign do
    sequence(:name) { |n| "name#{n}" }
    sequence(:link) { |n| "http://domain.com/link_#{n}" }
    image { File.open(File.join(Rails.root, 'spec', 'support', 'banner.gif')) }
  end

  factory :campaign_with_user, parent: :campaign do
    user
  end
end