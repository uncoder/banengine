FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user_#{n}@banengine.com" }
    sequence(:password) { |n| "password_#{n}" }
  end
end