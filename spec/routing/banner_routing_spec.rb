require 'rails_helper'

RSpec.describe "banner routing", :type => :routing do
  it "routes /banner.js to banner#index" do
    expect(get: "/banner.js").to route_to(controller: 'banner', action: 'index', format: "js")
  end

  it "routes /banner/1/click to banner#click" do
    expect(get: "/banner/1/click").to route_to(controller: 'banner', action: 'click', id: '1')
  end
end