require 'rails_helper'

RSpec.describe "campaigns routing", :type => :routing do
  it "routes /campaigns to campaigns#index" do
    expect(get: "/campaigns").to route_to(controller: 'campaigns', action: 'index')
  end

  it "routes /campaigns/new to campaigns#new" do
    expect(get: "/campaigns/new").to route_to(controller: 'campaigns', action: 'new')
  end

  it "routes /campaigns to campaigns#create" do
    expect(post: "/campaigns").to route_to(controller: 'campaigns', action: 'create')
  end

  it "routes /campaigns/1/edit to campaigns#edit" do
    expect(get: "/campaigns/1/edit").to route_to(controller: 'campaigns', action: 'edit', id: "1")
  end

  it "routes /campaigns/1 to campaigns#update" do
    expect(put: "/campaigns/1").to route_to(controller: 'campaigns', action: 'update', id: "1")
  end

  it "routes /campaigns/1 to campaigns#show" do
    expect(get: "/campaigns/1").to route_to(controller: 'campaigns', action: 'show', id: "1")
  end

  it "routes /campaigns/1 to campaigns#destroy" do
    expect(delete: "/campaigns/1").to route_to(controller: 'campaigns', action: 'destroy', id: "1")
  end
end