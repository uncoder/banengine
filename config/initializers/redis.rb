module Cache
  def self.redis
    db = Rails.configuration.redis[:db][:development]

    case Rails.env.to_s
    when "test"
      db = Rails.configuration.redis[:db][:test]
    when "production"
      db = Rails.configuration.redis[:db][:production]
    end

    Redis.new(host: Rails.configuration.redis[:host], port: Rails.configuration.redis[:port], db: db, driver: :hiredis)
  end
end