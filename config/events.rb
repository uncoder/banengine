WebsocketRails::EventMap.describe do

  namespace :websocket_rails do
    subscribe :subscribe_private, :to => CampaignsWsController, :with_method => :authorize_channels
  end
  
end
