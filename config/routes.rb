Rails.application.routes.draw do

  devise_for :users
  root 'welcome#index'

  resources :campaigns

  resources :banner, only: :index do
    get :click, on: :member
  end
  
end
