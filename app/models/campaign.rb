class Campaign < ActiveRecord::Base

  validates :user_id, presence: true
  validates :name, presence: true
  validates :link, presence: true, url: true
  validates :image, presence: true

  belongs_to :user

  mount_uploader :image, ImageUploader

  after_create do
    data = {
      hits: 0,
      views: 0,
      link: link
    }.to_a.flatten
    Cache.redis.hmset cache_key, data
    Cache.redis.sadd "campaigns_list", id
  end

  after_update do
    Cache.redis.hset cache_key, :link, link
  end

  after_destroy do
    Cache.redis.del cache_key
    Cache.redis.srem "campaigns_list", id
  end

  def cache_key
    "campaigns:#{id}"
  end

  def get_counts
    counts = Cache.redis.hmget cache_key, "views", "hits"
    views, hits = counts.map(&:to_f)
    if views > 0
      conv = hits / views * 100
    elsif hits > 0
      conv = 100
    else
      conv = 0
    end
    return {
      views: views.to_i,
      hits: hits.to_i,
      conversion: conv.round
    }
  end

  class << self

    def random_id
      Cache.redis.srandmember("campaigns_list").to_i
    end

    def inc_views id
      Cache.redis.hincrby "campaigns:#{id}", "views", 1
    end

    def inc_hits id
      Cache.redis.hincrby "campaigns:#{id}", "hits", 1
    end

    def get_link id
      Cache.redis.hget "campaigns:#{id}", "link"
    end
    
  end

end
