module BannerHelper

  def banner_hml_code id
    link_to click_banner_url(id: id) do
      image_tag root_url + "uploads/campaign/image/#{id}/banner.gif"
    end
  end

end
