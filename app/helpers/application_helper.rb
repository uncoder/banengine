module ApplicationHelper

  def flash_message
    html_msg = ""
    flash.each do |name, msg|
      case name.to_sym
      when :notice
        name = :success
      when :alert
        name = :danger
      when :error
        name = :danger
      end
      html_msg = content_tag :div, msg, :class => "alert alert-#{name}"
    end
    return html_msg
  end
  
end
