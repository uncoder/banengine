class CampaignsWsController < WebsocketRails::BaseController

  def authorize_channels
    channel = WebsocketRails[message[:channel]]
    unless current_user
      deny_channel(t(:access_denied))
    else
      if Campaign.exists?(id: channel.name, user_id: current_user.id)
        accept_channel current_user
      else
        deny_channel(t(:auth_failure))
      end
    end
  end

end