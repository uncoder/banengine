class BannerController < ApplicationController
  protect_from_forgery except: :index

  def index
    respond_to do |format|
      format.js do
        @id = Campaign.random_id
        views = Campaign.inc_views @id
        WebsocketRails[@id].trigger :update_views, { views: views }
      end
    end
  end

  def click
    link = Campaign.get_link params[:id]
    if link
      hits = Campaign.inc_hits params[:id]
      WebsocketRails[params[:id].to_i].trigger :update_hits, { hits: hits }
      redirect_to link
    else
      raise NoCampaign
    end
  end

  class NoCampaign < StandardError; end

end
