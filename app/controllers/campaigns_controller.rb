class CampaignsController < ApplicationController
  before_action :authenticate_user!

  def index
    @campaigns = current_user.campaigns
  end

  def new
    @campaign = Campaign.new
  end

  def create
    @campaign = Campaign.new user: current_user
    @campaign.update_attributes campaign_params
    if @campaign.save
      redirect_to campaigns_path, notice: t('campaigns.campaign_created')
    else
      render :new
    end
  end

  def edit
    @campaign = current_user.campaigns.find params[:id]
  end

  def update
    @campaign = current_user.campaigns.find params[:id]
    if @campaign.update_attributes campaign_params
      redirect_to campaigns_path, notice: t('campaigns.campaign_updated')
    else
      render :edit
    end 
  end

  def destroy
    @campaign = current_user.campaigns.find params[:id]
    @campaign.destroy
    redirect_to campaigns_path, notice: t('campaigns.campaign_deleted')
  end

  def show
    @campaign = current_user.campaigns.find params[:id]
    @counts = @campaign.get_counts
  end

  private

  def campaign_params
    params.require(:campaign).permit(:name, :link, :image)
  end
end
