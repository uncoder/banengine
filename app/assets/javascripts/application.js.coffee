#= require jquery
#= require jquery.turbolinks
#= require jquery_ujs
#= require turbolinks
#= require jquery.readyselector
#= require websocket_rails/main
#= require_tree .

$(document).ready ->
  window.ws_channel = null if $('.campaigns.show').length is 0
  LiveViews.dispatcher.disconnect() if window.LiveViews and not window.ws_channel