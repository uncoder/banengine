class @LiveViewsApp

  constructor: () ->
    @dispatcher = new WebSocketRails(window.location.host + "/websocket")
    @currentChannel = @dispatcher.subscribe_private(window.ws_channel)
    @currentChannel.bind 'update_views', @updateViews
    @currentChannel.bind 'update_hits', @updateHits
    @currentChannel.on_failure = (reason) ->
      alert reason

  updateViews: (data) =>
    $('#views_field').text data.views
    @updateConversion()

  updateHits: (data) =>
    $('#hits_field').text data.hits
    @updateConversion()

  updateConversion: () =>
    views = parseInt($('#views_field').text())
    hits = parseInt($('#hits_field').text())
    if views > 0
      conv = Math.round(hits / views * 100)
    else if hits > 0
      conv = 100
    else 
      conv = 0
    $('#conversion_field').text "#{conv}%"

$(".campaigns.show").ready ->
  window.LiveViews = new LiveViewsApp() if window.ws_channel