class CreateCampaigns < ActiveRecord::Migration
  def change
    create_table :campaigns do |t|
      t.belongs_to :user
      t.string :name
      t.string :link
      t.string :image

      t.timestamps
    end
  end
end
