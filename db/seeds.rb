user = User.create email: 'user@banengine.com', password: 'secretpass', password_confirmation: 'secretpass'
Campaign.create user: user, name: '1st campaign', link: 'http://google.com', image: File.open(File.join(Rails.root, 'spec', 'support', '1.gif'))
Campaign.create user: user, name: '2nd campaign', link: 'http://facebook.com', image: File.open(File.join(Rails.root, 'spec', 'support', '2.gif'))
Campaign.create user: user, name: '3rd campaign', link: 'http://github.com', image: File.open(File.join(Rails.root, 'spec', 'support', '3.gif'))